//
//  ContentView.swift
//  simply-find-ios-app
//
//  Created by William Arias on 24.02.2024.
//

import SwiftUI
import WebKit

struct ContentView: View {
    
    var body: some View {
        WebView(urlString: "https://resp-simply-find-l6bqixsvvq-uc.a.run.app/")
    }
}

struct WebView: UIViewRepresentable {
    
    let urlString: String
    
    func makeUIView(context: Context) -> WKWebView {
        let webView = WKWebView()
        guard let url = URL(string: urlString) else { return webView }
        let request = URLRequest(url: url)
        webView.load(request)
        return webView
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        
    }
}


struct ViewController_Previews: PreviewProvider {
    static var previews: some View {
        ContentView() 
    }
}

