//
//  simply_find_ios_appApp.swift
//  simply-find-ios-app
//
//  Created by William Arias on 24.02.2024.
//

import SwiftUI

@main
struct simply_find_ios_appApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
